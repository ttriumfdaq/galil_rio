
#include <iostream>

#include "TGalilRIO.h"

// Little test program that sets and reads the analog 
// output and input on Galil 47120. 
int main(int argc, const char * argv[]){

  TGalilRIO *rio = new TGalilRIO("192.168.1.100");


  // Do tests of setting output and reading input

  for(int i = -10; i < 10; i++){
    
    usleep(1000);
    
    float target = ((float)i)/2.0;
    rio->SetAnalogOutput(0,target);
    
    usleep(1000);
    
    float value = rio->GetAnalogInput(0);
    std::cout << "target value =" << target 
	      << ". readback value = " << value << std::endl;

  }


}


